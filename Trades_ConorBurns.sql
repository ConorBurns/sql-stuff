--question 1
/*Find all trades by a given trader on a given stock - for example the trader with ID=1 and the ticker 'MRK'. 
This involves joining from trader to position and then to trades twice, through the opening- and closing-trade FKs.*/
select t2.first_name, t.stock, case when t.buy = 1 then 'BUY' else 'SELL' end, t.size, t.price
from trade t
  join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
  join trader t2 on t2.ID = p.trader_ID
  where p.trader_id = 2 and t.stock = 'MRK'

--question 2
/*Find the total profit or loss for a given trader over the day, as the sum of the product
of trade size and price for all sales, minus the sum of the product of size and price for all buys.*/

--question 3
/*Develop a view that shows profit or loss for all traders.*/