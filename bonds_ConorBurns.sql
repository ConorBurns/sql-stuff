-- Question 1
/*Show all information about the bond with the CUSIP '28717RH95'.*/

select * from bond where CUSIP = '28717RH95'

-- Question 2
/*Show all information about all bonds, in order from shortest (earliest maturity) to longest (latest maturity).*/

select * from bond order by maturity 

-- Question 3
/*Calculate the value of this bond portfolio, as the sum of the product of each bond's quantity and price.*/

select CUSIP, sum(quantity * price) from bond group by CUSIP
--select sum(quantity * price) from bond

-- Question 4
/*Show the annual return for each bond, as the product of the quantity and the coupon. 
Note that the coupon rates are quoted as whole percentage points, 
so to use them here you will divide their values by 100.*/

select CUSIP, sum((quantity * price)/100 * coupon) from bond group by CUSIP 

-- Question 5
/*Show bonds only of a certain quality and above, 
for example those bonds with ratings at least AA2.
(Don't resort to regular-expression or other string-matching tricks; use the bond-rating ordinal.)*/
select CUSIP, rating from bond where rating in ('AAA', 'AA1', 'AA2')

-- Question 6
/*Show the average price and coupon rate for all bonds of each bond rating.*/
select rating, avg(price) as price, avg(coupon) as coupon from bond group by rating 

-- Question 7
/*Calculate the yield for each bond, as the ratio of coupon to price. 
Then, identify bonds that we might consider to be overpriced, 
as those whose yield is less than the expected yield given the rating of the bond.
*/

select b.CUSIP, b.coupon/b.price as AccYield, r.expected_yield
from bond b
join rating r on b.rating = r.rating
where b.coupon/b.price < r.expected_yield
